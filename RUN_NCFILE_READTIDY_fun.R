###======================================================================
###                                        Rafael Tieppo
###                                        rafaelt@unemat.br
###                                        https://rafatieppo.github.io/
###                                        10-11-2017
### Functions to Read and Tidy NC files
### For a specified coordinates, it gets meteorological data from XAVIER
### data base.
### 
###======================================================================

### load required package
#install.packages('ncdf4' )
library(ncdf4)
library(dplyr)
rm(list=ls())

# patt = c("ET", "pr", "RH", "Rs", "Tmax", "Tmin", "u2" );i=patt[1]
 patt = c("ET", "prec")

 ### assign coordinates and shortname
 LOCAL_LON = -50.60
 LOCAL_LAT = -27.28
 SH_NAME <-  "CBS"
 
 
TI <- Sys.time()
for (i in patt){

    ### assign the path to find the files from Xavier data base
    #PATH <- "C:/Users/joaob/OneDrive - UFSC Universidade Federal de Santa Catarina/_UFSC/Analises estatisticas/Fabio - xavier dados clima/"
    PATH <- getwd()
    
    ### load function
    source(paste0(PATH,"/NCFILE_READTIDY_fun2.R"))
    
    
    ### assign local name (shortname)
    LOCAL_NAME = paste(SH_NAME,i, sep="_")

    #VEC_FILES
    VEC_FILES <- list.files(PATH, pattern = c(i,".nc"))


#RUN
data <-  ncfile_fun(LOCAL_LON, LOCAL_LAT, LOCAL_NAME, VEC_FILES)

print(paste(i, "terminou"))
}


#juntar todos os pontos
files <- list.files(paste0(PATH,"/output"))
files <- files[grep(SH_NAME, files)]
files <- files[grep(".csv", files)]

n <- 0
arq <- list()
for (w in files) {
    # n <- n + 1
    arq[[which(files==w)]] <- read.csv(paste0(PATH, "/output/", w))
}


data <- arq[[1]]
for (k in 2:length(arq)) {
    data <- cbind(data, arq[[k]][, ])
}
data <- data[,!duplicated(colnames(data))]
data$TIMESTAMP <- as.Date(as.character(data$TIMESTAMP))

data <- data[-1:-2]

colnames(data)[1] <- "DATA"

rm(arq)

rio::export(data,paste0(PATH,
                        "/output/",
                        SH_NAME,
                        ".xlsx") )


TF <- Sys.time()
TT <- (TF - TI)
print(c('Total total time was', TT))
